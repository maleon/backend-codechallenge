<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        if (0 === strpos($pathinfo, '/driver')) {
            // driver_index
            if ('/driver' === $pathinfo) {
                $ret = array (  '_controller' => 'App\\Controller\\DriversController::indexAction',  '_route' => 'driver_index',);
                if (!in_array($canonicalMethod, array('GET'))) {
                    $allow = array_merge($allow, array('GET'));
                    goto not_driver_index;
                }

                return $ret;
            }
            not_driver_index:

            // driver_show
            if (preg_match('#^/driver/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, array('_route' => 'driver_show')), array (  '_controller' => 'App\\Controller\\DriversController::showAction',));
                if (!in_array($canonicalMethod, array('GET'))) {
                    $allow = array_merge($allow, array('GET'));
                    goto not_driver_show;
                }

                return $ret;
            }
            not_driver_show:

        }

        elseif (0 === strpos($pathinfo, '/order')) {
            // order_index
            if ('/order' === $pathinfo) {
                $ret = array (  '_controller' => 'App\\Controller\\OrdersController::indexAction',  '_route' => 'order_index',);
                if (!in_array($canonicalMethod, array('GET'))) {
                    $allow = array_merge($allow, array('GET'));
                    goto not_order_index;
                }

                return $ret;
            }
            not_order_index:

            // order_show
            if (preg_match('#^/order/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, array('_route' => 'order_show')), array (  '_controller' => 'App\\Controller\\OrdersController::showAction',));
                if (!in_array($canonicalMethod, array('GET'))) {
                    $allow = array_merge($allow, array('GET'));
                    goto not_order_show;
                }

                return $ret;
            }
            not_order_show:

            // order_store
            if ('/order' === $pathinfo) {
                $ret = array (  '_controller' => 'App\\Controller\\OrdersController::storeAction',  '_route' => 'order_store',);
                if (!in_array($requestMethod, array('POST'))) {
                    $allow = array_merge($allow, array('POST'));
                    goto not_order_store;
                }

                return $ret;
            }
            not_order_store:

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
