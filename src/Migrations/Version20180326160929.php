<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180326160929 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $ordersTable = $schema->getTable('orders');
        $driverTable = $schema->getTable('drivers');
        $ordersTable->addForeignKeyConstraint($driverTable,
            ['driver_id'],
            ['id'],
            ['onUpdate' => 'CASCADE', 'onDelete' => 'CASCADE'],
            'FK_DRIVER_ID_ORDERS'
        );
    }

    public function down(Schema $schema)
    {
        $table = $schema->getTable('orders');
        if ($table->hasForeignKey('FK_DRIVER_ID_ORDERS')) {
            $table->removeForeignKey('FK_DRIVER_ID_ORDERS');
        }
    }
}
