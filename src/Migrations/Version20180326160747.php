<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180326160747 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->createTable('orders');
        $table->addColumn('id', 'integer', [
            'unsigned'      => true, 
            'autoincrement' => true,
        ]);
        $table->addColumn('driver_id', 'integer', [
            'unsigned'  => true,
            'notnull'   => 1,
        ]);
        $table->addColumn('name', 'string', [
            'length'  => 25,
            'notnull' => 1,
        ]);
        $table->addColumn('surname', 'string', [
            'length'  => 50,
            'notnull' => 1,
        ]);
        $table->addColumn('email', 'string', [
            'length'  => 75,
            'notnull' => 1,
        ]);
        $table->addColumn('phones', 'string', [
            'length'  => 100,
            'notnull' => 1,
        ]);
        $table->addColumn('address', 'string', [
            'length'  => 100,
            'notnull' => 1,
        ]);
        $table->addColumn('date', 'date', [
            'notnull' => 1,
        ]);
        $table->addColumn('time', 'time', [
            'notnull' => 1,
        ]);
        $table->addColumn('estimated', 'integer', [
            'unsigned'  => 1,
            'default'   => 0,
        ]);
        $table->setPrimaryKey(['id']);
    }

    public function down(Schema $schema)
    {
        $schema->dropTable('orders');
    }
}
