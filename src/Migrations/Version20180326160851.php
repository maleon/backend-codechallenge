<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180326160851 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->createTable('drivers');
        $table->addColumn('id', 'integer', [
            'unsigned'      => true, 
            'autoincrement' => true,
        ]);
        $table->addColumn('name', 'string', [
            'length'  => 25,
            'notnull' => 1,
        ]);
        $table->addColumn('surname', 'string', [
            'length'  => 50,
            'notnull' => 1,
        ]);
        $table->addColumn('code', 'string', [
            'length'  => 9,
            'notnull' => 1,
        ]);
        $table->addColumn('phone', 'integer', [
            'notnull' => 1,
        ]);
        $table->setPrimaryKey(['id']);
    }

    public function down(Schema $schema)
    {
        $schema->dropTable('drivers');
    }
}
