<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Annotation\Method;

use App\Entity\Order;
use App\Entity\Driver;

class OrdersController extends Controller
{
    /**
     * Matches /order exactly
     *
     * @Route("/order", name="order_index", methods={"GET"})
     */
    public function indexAction()
    {
        return ;
    }

    /**
     * Matches /order/*
     *
     * @Route("/order/{id}", name="order_show", methods={"GET"})
     */
    public function showAction($id)
    {
        return ;
    }

    /**
     * Matches /order exactly
     *
     * @Route("/order", name="order_store", methods={"POST"})
     */
    public function storeAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        
        if (!Order::validate($data)) {
            return $this->json(['error' => 'Missing data']);
        }
        $entityManager = $this->getDoctrine()->getManager();

        $order = new Order();
        $order->setName($data['name']);
        $order->setSurname($data['surname']);
        $order->setEmail($data['email']);
        $order->setPhones($data['phones']);
        $order->setAddress($data['address']);
        $order->setDate($data['date']);
        $order->setTime($data['time']);
        $estimated = 0;
        if (isset($data['estimated']) && !empty($data['estimated'])) {
            $estimated = $data['estimated'];
        }
        $order->setEstimated($estimated);

        $rd = $this->getDoctrine()->getRepository(Driver::class);
        $count = $rd->count([]);
        $key = rand(0,100)%$count+1;
        $driver = $rd->find($key);
        $order->setDriver($driver);
        $entityManager->persist($order);
        $entityManager->flush();

        return $this->json(['data' => print_r($order, true)]);
    }
}