<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Annotation\Method;

use App\Entity\Order;
use App\Entity\Driver;

class DriversController extends Controller
{
    /**
     * Matches /driver exactly
     *
     * @Route("/driver", name="driver_index", methods={"GET"})
     */
    public function indexAction()
    {
        return ;
    }

    /**
     * Matches /driver/*
     *
     * @Route("/driver/{id}", name="driver_show", methods={"GET"})
     */
    public function showAction($id)
    {
        $rd = $this->getDoctrine()->getRepository(Driver::class);
        $driver = $rd->find($id);

        if (!$driver instanceof Driver) {
            throw $this->createNotFoundException('No driver found for id '.$id);
        }

        foreach ($driver->getOrders()->toArray() as $order) {
            $orders []= $order->toArray();
        }
        
        return $this->json(['data' => $orders]);
    }
}