<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;

use Doctrine\Common\Collections\Collection;


/**
 * App\Entity\Driver
 *
 * @ORM\Table(name="drivers")
 * @ORM\Entity
 */
class Driver
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="id", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=25, nullable=false)
     */
    private $name;

    /**
     * @var string $surname
     *
     * @ORM\Column(name="surname", type="string", length=50, nullable=false)
     */
    private $surname;

    /**
     * @var string $code
     *
     * @ORM\Column(name="code", type="string", length=9, nullable=false)
     */
    private $code;

    /**
     * @var string $phone
     *
     * @ORM\Column(name="phone", type="integer", nullable=false)
     */
    private $phone;

    /**
     * @var string $orders
     *
     * @ORM\OneToMany(targetEntity="Order", mappedBy="driver")
     */
    private $orders;

    /**
     * @return Collection|Order[]
     */
    public function getOrders()
    {
        return $this->orders;
    }
}