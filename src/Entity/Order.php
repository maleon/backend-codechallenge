<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;

/**
 * App\Entity\Order
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity
 */
class Order
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="id", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var driver
     *
     * @ORM\ManyToOne(targetEntity="Driver", inversedBy="orders")
     * @ORM\JoinColumn(name="driver_id", referencedColumnName="id")
     */
    private $driver;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=25, nullable=false)
     */
    private $name;

    /**
     * @var string $surname
     *
     * @ORM\Column(name="surname", type="string", length=50, nullable=false)
     */
    private $surname;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=75, nullable=false)
     */
    private $email;

    /**
     * @var string $phones
     *
     * @ORM\Column(name="phones", type="string", length=100, nullable=false)
     */
    private $phones;

    /**
     * @var string $address
     *
     * @ORM\Column(name="address", type="string", length=100, nullable=false)
     */
    private $address;

    /**
     * @var string $date
     *
     * @ORM\Column(name="date", type="string", length=8, nullable=false)
     */
    private $date;

    /**
     * @var string $time
     *
     * @ORM\Column(name="time", type="string", length=8, nullable=false)
     */
    private $time;

    /**
     * @var string $estimated
     *
     * @ORM\Column(name="estimated", type="integer", nullable=false)
     */
    private $estimated;

    public function getDriver()
    {
        return $this->driver;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPhones()
    {
        return $this->phones;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function getEstimated()
    {
        return $this->estimated;
    }

    public function setDriver($value = '')
    {
        $this->driver = $value;
        return $this;
    }

    public function setName($value = '')
    {
        $this->name = $value;
        return $this;
    }

    public function setSurname($value = '')
    {
        $this->surname = $value;
        return $this;
    }

    public function setEmail($value = '')
    {
        $this->email = $value;
        return $this;
    }

    public function setPhones($value = '')
    {
        $this->phones = $value;
        return $this;
    }

    public function setAddress($value = '')
    {
        $this->address = $value;
        return $this;
    }

    public function setDate($value = '')
    {
        $this->date = $value;
        return $this;
    }

    public function setTime($value = '')
    {
        $this->time = $value;
        return $this;
    }

    public function setEstimated($value = '')
    {
        $this->estimated = $value;
        return $this;
    }

    static public function validate(Array $data = [])
    {
        if (!isset($data['name']) || empty($data['name'])) {
            return false;
        }

        if (!isset($data['surname']) || empty($data['surname'])) {
            return false;
        }

        if (!isset($data['email']) || empty($data['email'])) {
            return false;
        }

        if (!isset($data['phones']) || empty($data['phones'])) {
            return false;
        }

        if (!isset($data['address']) || empty($data['address'])) {
            return false;
        }

        if (!isset($data['date']) || empty($data['date'])) {
            return false;
        }

        if (!isset($data['time']) || empty($data['time'])) {
            return false;
        }

        return true;
    }

    public function toArray()
    {
        return [
            'name'      => $this->name,
            'surname'   => $this->surname,
            'email'     => $this->email,
            'phones'    => $this->phones,
            'address'   => $this->address,
            'date'      => $this->date,
            'time'      => $this->time,
            'estimated' => $this->estimated,
        ];
    }
}